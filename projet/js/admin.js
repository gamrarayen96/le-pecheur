let form = document.getElementById("form");
let btn = document.getElementById("btn");
let title = document.getElementById("title");
let description = document.getElementById("description");
let price = document.getElementById("price");
let err = document.getElementById("err");
var tbody = document.querySelector("#tbody");
var recherche = document.querySelector("#recherche");
var isUpdate = false;
var data;
var position = -1;
if (localStorage.length == 0) {
  data = [];
} else {
  data = JSON.parse(localStorage.data);
}
//or
// window.onload = () => {
//   if (localStorage.length == 0) {
//     data = [];
//   } else {
//     data = JSON.parse(localStorage.data);
//   }
// };

showData();

form.onsubmit = () => {
  if (title.value == "") {
    err.classList.remove("hidden");
  } else {
    if (!isUpdate) {
      let product = {};
      product.title = title.value;
      product.description = description.value;
      product.price = price.value;
      data.push(product);
    } else {
      data[position].title = title.value;
      data[position].description = description.value;
      data[position].price = price.value;
      btn.innerText = "save";
    }
    resetForm();
    showData();
    saveData();
  }
};

function showData() {
  let table = "";
  for (let index = 0; index < data.length; index++) {
    table += `
        <tr class="mt-0 border-b  text-base font-semibold text-gray-800 ">
        <td class="py-3">${index + 1}</td>
        <td>${data[index].title}</td>
        <td>${data[index].description}</td>
        <td>${data[index].price}</td>
        <td class="flex gap-4 py-2">
          <button onclick="removeProduct(${index})" type="button" id="btn" class="bg-red-700 text-white hover:bg-red-500 w-full  hover:shadow-2xl py-1 rounded-lg">delete</button>
          <button onclick="updateData(${index})" type="button" id="btn" class="bg-gray-700 text-white hover:bg-slate-500 w-full  hover:shadow-2xl py-1 rounded-lg">update</button>
        </td>
      </tr>  
        `;
  }
  tbody.innerHTML = table;
}

function showData() {
  let table = "";
  for (let index = 0; index < data.length; index++) {
    table += `
        <tr class="mt-0 border-b  text-base font-semibold text-gray-800 ">
        <td class="py-3">${index + 1}</td>
        <td>${data[index].title}</td>
        <td>${data[index].description}</td>
        <td>${data[index].price}</td>
        <td class="flex gap-4 py-2">
          <button onclick="removeProduct(${index})" type="button" id="btn" class="bg-red-700 text-white hover:bg-red-500 w-full  hover:shadow-2xl py-1 rounded-lg">delete</button>
          <button onclick="updateData(${index})" type="button" id="btn" class="bg-gray-700 text-white hover:bg-slate-500 w-full  hover:shadow-2xl py-1 rounded-lg">update</button>
        </td>
      </tr>  
        `;
  }
  tbody.innerHTML = table;
}
function resetForm() {
  title.value = "";
  description.value = "";
  price.value = "";
}

function saveData() {
  localStorage.setItem("data", JSON.stringify(data));
}

function removeProduct(i) {
  console.log(i);
  data.splice(i, 1);
  saveData();
  showData();
}

function updateData(i) {
  title.value = data[i].title;
  description.value = data[i].description;
  price.value = data[i].price;
  isUpdate = true;
  position = i;
  btn.innerText = "update";
}

function removeData(params) {
  localStorage.clear();
  data.splice(0);
  showData();
}

recherche.onkeyup = () => {
  console.log(recherche.value);
  let newData = [];
  for (let index = 0; index < data.length; index++) {
    if (
      data[index].title.includes(recherche.value) ||
      data[index].description.includes(recherche.value)
    ) {
      newData.push(data[index]);
    }
  }
  // newData = data.filter((e)=>e.title.includes(recherche.value))
  data = newData;
  showData();
};

